class TestBugG3 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
        boolean x, y;
        x = false;
        y = true;
        while (y) {
            System.out.println(0);
            y = x;
        }
        return y;
    }

}
