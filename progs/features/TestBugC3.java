class TestBugC3 {
    public static void main(String[] a) {
        System.out.println(new Test().f());

    }
}

class Test {

    public int f() {
        int x, y, z;
        x = 10;
        y = 20;
        z = 20;
        if (x <= y && y <= z) {
            System.out.println(1);
        } else {
            System.out.println(2);
        }
        return 0;
    }

}
